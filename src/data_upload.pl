#!/usr/bin/perl -w

# https://www.sitepoint.com/uploading-files-cgi-perl-2
# upload.cgi

# https://www.grm.cuhk.edu.hk/~htlee/perlcourse/fileupload/fileupload2.html

# sudo apt install apache2 libapache2-mod-perl2 libnet-ldap-perl 

use strict;
use CGI; # see https://metacpan.org/pod/CGI
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use Net::LDAP;          # libnet-ldap-perl          for ldap user check
use File::Copy;
use File::Path;

$CGI::POST_MAX        = 50*1024*1024*1024; # in bytes, default 100kB
$CGI::DISABLE_UPLOADS = 0;        # 1 disables uploads, 0 enables uploads

# CONFIG STORAGE: where to store. Must exist with proper permissions.
my $www_root      = "/var/www/html";
my $target        = "data/ai";
my $ldap_uri      = '';       # e.g. 'ldap://195.221.10.1' or 'ldap://example.com:389' or ''
my $ldap_base     = 'dc=EXP'; # e.g. 'dc=EXP' or 'dc=example,dc=com' or ''

# get the form
my $form          = new CGI;
my $error         = "";

# test form consistency
my @fields   = ( 'user_id','dataset_name','dataset_origin','dataset_annotation','dataset_description','dataset');

# test if the form has the required fields
for (@fields) {
  if ( not defined $form->param($_) ) {
    $error .= "Incomplete form: fields are missing: $_";
  }
}
# test if required information is there
for ('user_id','dataset_name') {
  if (not $form->param($_)) {
    $error .= "Some required (*) information is missing: $_";
  }
}
# test for valid credentials (when LDAP is defined)
my $authenticated = session_check_ldap(
                      $ldap_uri, $ldap_base,
                      $form->param('user_id'), $form->param('user_pw'));
if (index($authenticated, "FAILED") > -1) {
  $error .= $authenticated;
}

# get a valid dataset_name (will be a directory)
my $dataset_name = $form->param('dataset_name');
my $safe_filename_characters    = "a-zA-Z0-9_.-";
$dataset_name    =~ s/[^$safe_filename_characters]//g; # remove illegal characters

my $servername    = $form->server_name();
if ($servername =~ "::1") { $servername = "localhost"; }

print STDERR "$0: INFO: Uploading $dataset_name to $www_root/$target\n";

print $form->header ( ); # wrap up in html
print "<TITLE>SOLEIL/GRADES: AI Dataset Upload</TITLE>\n";
print "<H1>Data Upload: $dataset_name</H1>\n";

# upload each given dataset
my @datasets    = $form->param('dataset');
my @io_handles  = $form->upload('dataset');
my $dataset_file_nb = 0;

# create a YAML content
my $url = $ENV{'HTTP_ORIGIN'} . "/$target/$dataset_name";
my $yaml = "# YAML\n";
$yaml .= "location: $url\n";
$yaml .= "date: " . localtime() . "\n";
$yaml .= "user_ip: " . $ENV{'REMOTE_HOST'} . " [" . $ENV{'REMOTE_ADDR'} . "]\n";
foreach my $f (@fields) {
  my $val = $form->param($f);
  $yaml .= "$f: $val\n";
}
$yaml .= "files:\n";

if (not $error) {
  print '<ol>';
  foreach my $file (@datasets) { # scan file names
      my $filesafe = $file;
      $filesafe =~ s/[^$safe_filename_characters]//g; # remove illegal characters
      my $handle     = shift @io_handles; # get uploaded file handle, in order
      if (defined $handle) {
        File::Path::make_path("$www_root/$target/$dataset_name");  # make sure target exists
        # move the temp file to the storage we use
        if (copy($form->tmpFileName( $handle ), "$www_root/$target/$dataset_name/$filesafe")) {
          print "<li>$filesafe</li>\n";
          $yaml .= "    - $filesafe\n";
          $dataset_file_nb++;
        } else {
          print "<li><i>$file</i> (failed upload, ignored)</li>\n";
        }
      }
  }
  print "</ol><br>";
}

# generate the text to display/write
if (not $error and $dataset_file_nb) {
  print "<h2>The data has been uploaded in <a href='$url'>$url</a></h2>\n";
  print "<pre>\n$yaml\n</pre>\n";
  
  # write the README.html file (with the YAML)
  open (FH, '>>', "$www_root/$target/$dataset_name/README.html") or die $!;
  print FH "<h1>Dataset $dataset_name</h1>\n";
  print FH "<p>Stored in <a href='$url'>$url</a></p>\n";
  print FH "<pre>\n";
  print FH "$yaml\n";
  print FH "</pre><hr>\n";
  close(FH);
  chmod 0644, "$www_root/$target/$dataset_name/README.html";
  open (FH, '>>', "$www_root/$target/$dataset_name/README.yml") or die $!;
  print FH "$yaml\n";
  close(FH);
  chmod 0644, "$www_root/$target/$dataset_name/README.yml";
  
  $url = $ENV{'HTTP_ORIGIN'} . "/$target";
  print "\n<hr><p>All data sets are available in <a href='$url'>$url</a></p>\n";
} else {
  print "<h1>ERROR uploading data set</h1>\n";
  print "<h2>\n";
  print "<p><div style='color:red'>$error</div></p>";
  print "</h2>\n";
}






# ------------------------------------------------------------------------------
# session_check_ldap((ldap_server, ldap_base, user, password):
#   input:
#     ldap_server: an IP/URI        e.g. 'ldap://195.221.10.1:389'
#     ldap_base:   a search string, e.g. 'dc=EXP'
#     user:        a user name,     e.g. 'farhie'
#     password:    the user pw
#   return ""         when no check is done
#          "FAILED"   when authentication failed
#          "SUCCESS"  when authentication succeeded
sub session_check_ldap {
  my $ldap_server = shift;
  my $ldap_base   = shift;
  my $user        = shift;
  my $password    = shift;
  my $res = '';
  
  if (not $ldap_server or not $ldap_base) { return ""; }

  if (not $user) {
    return "FAILED: Missing Username.";
  }
  
  my $ldap = Net::LDAP->new($ldap_server)
    or return "FAILED: Cannot connect to LDAP server '$ldap_uri': $@";
    
  # identify the DN
  my $mesg = $ldap->search(
    base   => "$ldap_base",
    filter => "cn=$user", # may also be "uid=$user"
    attrs  => ['dn','mail','cn','uid']);
  
  if (not $mesg or not $mesg->count) {
    $res = "FAILED: Empty LDAP search.\n";
  } else {
    foreach my $entry ($mesg->all_entries) {
      my $dn    = $entry->dn();
      my $cn    = $entry->get_value('cn'); # may also be 'uid'
      my $bmesg = $ldap->bind($dn,password=>$password);
      if ( $bmesg and $bmesg->code() == 0 ) {
        $res = "SUCCESS: $user authenticated.";
      }
      else{
        my $error = $bmesg->error();
        $res = "FAILED: Wrong username/password (failed authentication): $error\n";
      }
    }
  }
  $ldap->unbind;
  return $res;
  
} # session_check_ldap

