# data-upload-service

A web service that works on top of Apache in order to upload annotated data sets for AI training.

![Data Upload logo](images/data_upload-logo.png)

## Installation

First install the required libraries. On a Debian-class system, use:
```
sudo apt install apache2 libapache2-mod-perl2 libnet-ldap-perl 
sudo a2enmod cgid
sudo systemctl restart apache2
```

Then install the service itself:

- Tune the `src/data_upload.pl` configuration section. For instance the storage location is set to `data/ai`. This means that the `/var/www/html/data/ai` will be used physically. Also set the maximum allowed file size `POST_MAX`
- If you use an LDAP service to check user credentials, specify the URI `ldap_uri` and the domain/base search `ldap_base`. The password field will then be used for authentication.
- If you don't use the LDAP authenticator, you may comment the `input type="password"` section in the `src/data_upload.html` file, and leave the `ldap_uri` empty.
- Copy the `src/data_upload.html` page for instance as `/var/www/html/data/README.html` or `/var/www/html/data/data_upload.html`.
- Copy the `src/data_upload.pl` in `/usr/lib/cgi-bin`.


## Usage

Open a browser at e.g.:

- http://localhost/data/README.html (depending where the data_upload.html has been copied/renamed)

Uploaded data sets are stored locally e.g. in `/var/www/html/data/ai', and available from a browser at:

- http://localhost/data/ai

![landing page](images/data_upload.png)

![generated entry](images/data_upload_entry.png)

## Usage via an API call

It is possible to programmatically send a request for a computation with a command such as:
```
curl --trace-ascii LOGFILE -F user_id=farhie -F user_pw='xxxxx' -F dataset_name=my_AI_dataset -F dataset_origin=DISCO -F dataset_annotation="annotated images with classes" -F dataset_description="A set of images acquired at DISCO for which a manual annotation has been assigned." -F 'dataset=@"/path/to/dataset.zip"' https://data-analysis.synchrotron-soleil.fr/cgi-bin/data_upload.pl
```

The result is returned in `LOGFILE` (can be set as `/dev/stdout`), with lines (can span on two lines to be merged) at the end such as:
```
0000: Location: /data/ai/my_AI_dataset/
```

which indicates the location of the calculation on the server. Append this path `/data/ai/my_AI_dataset` to the server, here `https://data-analysis.synchrotron-soleil.fr/`.


## Credits

(c) Synchrotron SOLEIL, France. GPL3 license.
See https://gitlab.com/soleil-data-treatment/soleil-software-projects/data-upload-service
